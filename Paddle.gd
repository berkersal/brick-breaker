extends KinematicBody2D

var mouse = Vector2(0, position.y)
var size = null

func _ready():
	size = get_child(1).texture.get_size()

func _process(_delta):
	mouse.x = get_global_mouse_position()[0]

	if mouse.x < size.x + 16:
		mouse.x = size.x + 16
	elif mouse.x > get_viewport_rect().size.x - size.x - 16:
		mouse.x = get_viewport_rect().size.x - size.x - 16

	position = mouse
