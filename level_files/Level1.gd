extends Node2D

var location = "res://level_files/"
var brickCount = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	location += name.replace(" ", "") + ".txt"
	get_file(location)


#	pass
func get_file(file):
	var f = File.new()
	f.open(file, File.READ)
	var index = 1
	while not f.eof_reached(): # iterate through all lines until the end of file is reached
		var line = f.get_line()
		var lineArray = line.split(",", true)
		for i in lineArray:
			if i == "":
				i = 0
			new_brick(int(i), index)
		index += 1
	f.close()
	return

func new_brick(type, index):
	
	#BODY
	var brick = StaticBody2D.new()
	brick.position = Vector2((brickCount % 20) * 48 + 24,index * 16 - 8)
	brick.name = "Brick " + String(brickCount)
	brickCount += 1
	add_child(brick)
	
	if type == 0:
		return
	
	#SPRITE
	var sprite = Sprite.new()
	var texture = "res://brick_files/brick" + String(type) + ".png"
	sprite.texture = load(texture)
	sprite.position = brick.position
	brick.add_child(sprite)
	
	#SHAPE
	var shape = RectangleShape2D.new()
	shape.set_extents(sprite.texture.get_size() / 2)
	
	#COLLISION SHAPE
	var collision = CollisionShape2D.new()
	collision.shape = shape
	collision.position = brick.position
	brick.add_child(collision)
