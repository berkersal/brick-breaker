extends KinematicBody2D

const UP = Vector2(0, -1)
var motion = Vector2(0, -100)

#func _ready():
#	position.x = 484

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):

	if is_on_ceiling() or is_on_floor():
		motion.y = - motion.y
	elif is_on_wall():
		motion.x = -motion.x

	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider == null:
			continue
		elif collision.collider.name.substr(0, 5) == "Brick":
			print("************ BRICK COLLISION ************")
			collision.collider.get_parent().remove_child(collision.collider)
			collision.collider.free()
		elif collision.collider.name.substr(0, 6) == "Paddle":
			var paddlePos = collision.collider.position
			#motion.x = position.x - paddlePos.x
		elif collision.collider.name == "DownWall":
			get_tree().quit()
		else:
			print("--------- UNKNOWN COLLISION ----------")

	move_and_slide(motion, UP)
