extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var colliders = get_colliding_bodies()
	if colliders.size() != 0:
		#print(colliders[0].name)
		if colliders[0].name.substr(0, 5) == "Brick":
			colliders[0].get_parent().remove_child(colliders[0])
			colliders[0].free()
		elif colliders[0].name.substr(0, 6) == "Paddle":
			apply_impulse(Vector2(0, 0), Vector2((position.x - colliders[0].position.x) / 2500, 0))
		elif colliders[0].name == "DownWall":
			get_tree().quit()
